<%@page import="perf.RunJmeter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String mode = (String)session.getAttribute("mode");
	if (mode == null) {
		out.print("ERROR : False entry");
		return;
	} else if (mode.equals("offline")) {
		// 需要进行访问限制的时候放开下面两行
		out.print("ERROR : Unauthorized view");
		return;
	} else if (mode.equals("online")) {
		// 这里啥也不用做
	} else {
		out.print("ERROR : Unknown error");
		return;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		// 强制停止
		RunJmeter.stop();
		// 返回上一级
		request.getRequestDispatcher("selectScript.jsp").forward(request, response);
	%>
</body>
</html>