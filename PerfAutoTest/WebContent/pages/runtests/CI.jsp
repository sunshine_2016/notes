<%@page import="utils.EMailUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="perf.JmeterUtils"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.util.Properties"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="perf.ConfigUtils"%>
<%@page import="perf.Interface"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="perf.RunJmeter"%>
<%
	String mode = (String)session.getAttribute("mode");
	if (mode == null) {
		out.print("ERROR : False entry");
		return;
	} else if (mode.equals("offline")) {
		// 需要进行访问限制的时候放开下面两行
		out.print("ERROR : Unauthorized view");
		return;
	} else if (mode.equals("online")) {
		// 这里啥也不用做
	} else {
		out.print("ERROR : Unknown error");
		return;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
body {
	border: 1px #BBBBBB;
	border-bottom-style: solid;
	font-family: 宋体;
	color: #4A6EAA;
}

a {
	text-decoration: none;
	color: #4A6EAA;
	font-size: 20px;
}

a.atag:hover {
	color: red;
}
</style>
</head>
<body>
	<%!String[] interfaceNames;%>
	<%
	
		// 记录一下当前的服务器地址:端口，以便将url写入到邮件中
		final String urlPath = "http://" + request.getHeader("host") + "/PerfAutoTest";
	
		if (RunJmeter.runFlag) {
			out.println("ERROR : 测试已在进行中<br><br>");
		} else {
			
			interfaceNames = request.getParameterValues("interfaceName");
			if (interfaceNames == null) {
				out.println("INFO : 请至少选择一个接口测试<br><br>");
			} else {
				new Thread() {
					public void run() {
						try {
							while (true) {
								// 因为CI是定时任务，所以在这里手动将RunJmeter.runFlag置为true
								RunJmeter.runFlag = true;
								RunJmeter.isCI = true;
								RunJmeter.threads.add(Thread.currentThread());
								
								System.out.println(new SimpleDateFormat("HH").format(new Date()));
								
								while (!new SimpleDateFormat("HH").format(new Date()).equals(ConfigUtils.getCongByName("CITime"))) {
									Thread.sleep(1000 * 60);	
								}
								
								File CIinfo = new File(ConfigUtils.rootPath + "/logs/CI.info");
								String line = "";
								
								// 开始测试之前读取并清空一下CI.info文件
								BufferedReader readerbefore = new BufferedReader(new InputStreamReader(new FileInputStream(CIinfo), "utf-8"));
								Properties beforeProperties = new Properties();
								while ((line = readerbefore.readLine()) != null) {
									String[] lines = line.split("=");
									beforeProperties.put(lines[0], lines[1]);
								}
								readerbefore.close();
								
								System.out.print("删除老的CIinfo文件 : " + CIinfo.delete());
								System.out.print("重新创建CIinfo文件 : " + CIinfo.createNewFile());
								
								for (String interfaceName : interfaceNames) {
									new RunJmeter(ConfigUtils.getInterfaceByName(interfaceName)).start();
								}
								
								
								// 结束测试之后读取当前的CI.info，与之前进行对比，创建邮件并发送
								// 不发邮件了
								/*
								ArrayList<String> keys = new ArrayList<String>();
								
								BufferedReader readerafter = new BufferedReader(new InputStreamReader(new FileInputStream(CIinfo), "utf-8"));
								Properties afterProperties = new Properties();
								while ((line = readerafter.readLine()) != null) {
									String[] lines = line.split("=");
									keys.add(lines[0]);
									afterProperties.put(lines[0], lines[1]);
								}
								readerafter.close();
								
								StringBuffer message = new StringBuffer();
								
								message.append("<table border='1' align='left' style='font-size: 15px; border-collapse: collapse;'>");
								message.append("<tr><td>接口名</td><td>今日数据</td><td>昨日数据</td></tr>\r\n");
								
								for (String key : keys) {
									message.append("<tr><td>"
									+ key + "</td><td>"
									+ afterProperties.getProperty(key) + "</td><td>"
									+ beforeProperties.getProperty(key, "NA") + "</td></tr>");
								}
								
								message.append("</table>");
								message.append("<br>");
								message.append("<a href='" + urlPath + "'>查看详情</a>");
								EMailUtils eMailUtils = new EMailUtils(message.toString());
								eMailUtils.sendMessage();
								*/
								
								
								// 防止一个小时内运行多次
								Thread.sleep(1000 * 60 * 60);
							}
						} catch (Exception e) {e.printStackTrace();}
					}
				}.start();	
			}
		}
	%>
	<br />
	<br />
	<!-- 新窗口打开 -->
	<a class="atag" href="/PerfAutoTest/runLog.html" target="view_window">执行进度</a>
	<br />
	<br />
	<a class="atag" href="selectScript.jsp" target="detailpage">返回</a>
	<br />
	<br />
</body>
</html>