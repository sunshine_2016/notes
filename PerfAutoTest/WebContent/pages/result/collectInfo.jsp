<%@page import="perf.ConfigUtils"%>
<%@page import="perf.ExcelsToJson"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="/PerfAutoTest/jquery.js"></script>
<script src="g2.js"></script>
<script src="my.js"></script>
<style type="text/css">
p {
	font-size: 25px;
	text-align: center;
	color: #4A6EAA;
}

table {
	font-size: 18px;
	border-collapse: collapse;
}

div {
	font-size: 15px;
    color: #4A6EAA;
}
</style>
</head>
<body>
<%
	out.println(new ExcelsToJson().analyseExcels(request.getParameter("version"), ConfigUtils.getCongByName("lastVersion")));
%>
</body>
