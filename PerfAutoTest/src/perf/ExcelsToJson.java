package perf;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;

public class ExcelsToJson {

	private Logger logger = Logger.getLogger(ExcelsToJson.class);
	
	/**
	 * 解析选择的版本excel文件，与上一个版本（还不知道写在什么地方）进行对比
	 * */
	public String analyseExcels(String excelName, String lastExcelName) {
		
		final String name = excelName;
		final String lastName = lastExcelName;
		logger.info("ExcelsToJson: " + name);
		
		final List<InterfacesPerfInfo> interfacesPerfInfos = new ArrayList<InterfacesPerfInfo>();
		
		try {
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(new File(ConfigUtils.rootPath + "/excels/" + name + ".xls")));
			HSSFWorkbook lastWorkbook = null;
			if (new File(ConfigUtils.rootPath + "/excels/" + lastName + ".xls").exists()) {
				lastWorkbook = new HSSFWorkbook(new FileInputStream(new File(ConfigUtils.rootPath + "/excels/" + lastName + ".xls")));
			}
			
			Iterator<Sheet> iterator = workbook.sheetIterator();
			final CountDownLatch countDownLatch = new CountDownLatch(workbook.getNumberOfSheets());
			while (iterator.hasNext()) {
				
				final HSSFSheet sheet = (HSSFSheet)iterator.next();
				final HSSFSheet lastSheet;
				if (lastWorkbook != null) {
					lastSheet = lastWorkbook.getSheet(sheet.getSheetName());
				} else {
					lastSheet = null;
				}
				
				new Thread() {
					public void run() {
						double tpsMAX = 0.0;
						double lastTpsMAX = 0.0;
						
						InterfacesPerfInfo interfacesPerfInfo = new InterfacesPerfInfo();
						interfacesPerfInfo.interfaceName = sheet.getSheetName();
						interfacesPerfInfo.expectTPS = ConfigUtils.getInterfaceByDescTest(interfacesPerfInfo.interfaceName).expectTPS;
						
						for (int i = 2; i < 100; i++) {
							try {
								tpsMAX = sheet.getRow(i).getCell(0).getNumericCellValue();
								if (tpsMAX == 0.0) {
									throw new Exception();
								}
							} catch (Exception e) {
								logger.info("version: " + name + " sheetName: " + sheet.getSheetName() + " TPSMAX: " + tpsMAX);
								interfacesPerfInfo.nowTPS = sheet.getRow(i - 1).getCell(0).getNumericCellValue() + "";
								interfacesPerfInfo.nowTPSdelay = sheet.getRow(i - 1).getCell(1).getNumericCellValue() + "";
								break;
							}
						}
						
						if (lastSheet != null) {
							for (int i = 2; i < 100; i++) {
								try {
									lastTpsMAX = lastSheet.getRow(i).getCell(0).getNumericCellValue();
									if (lastTpsMAX == 0.0) {
										throw new Exception();
									}
								} catch (Exception e) {
									logger.info("LASTversion: " + lastName + " LASTsheetName: " + lastSheet.getSheetName() + " LASTTPSMAX: " + lastTpsMAX);
									interfacesPerfInfo.lastTPS = lastSheet.getRow(i - 1).getCell(0).getNumericCellValue() + "";
									interfacesPerfInfo.lastTPSdelay = lastSheet.getRow(i - 1).getCell(1).getNumericCellValue() + "";
									break;
								}
							}
						} else {
							interfacesPerfInfo.lastTPS = "NA";
							interfacesPerfInfo.lastTPSdelay = "NA";
						}
						
						interfacesPerfInfos.add(interfacesPerfInfo);
						countDownLatch.countDown();
					};
				}.start();
				
			}
			
			logger.info("ExcelsToJson - 开始等待");
			countDownLatch.await();
			logger.info("ExcelsToJson - 结束等待");
			
			workbook.close();
			lastWorkbook.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		Collections.sort(interfacesPerfInfos);
		
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append("<p>" + name + "</p>");
		stringBuffer.append("<div>");
		stringBuffer.append("<table border='1' align='center'>");
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Interface</td>");
		stringBuffer.append("<td>Now_TPS</td>");
		stringBuffer.append("<td>Now_Delay</td>");
		stringBuffer.append("<td>Last_TPS</td>");
		stringBuffer.append("<td>Last_Delay</td>");
		stringBuffer.append("<td>ExpectTPS</td>");
		stringBuffer.append("</tr>");
		
		Iterator<InterfacesPerfInfo> iterator = interfacesPerfInfos.iterator();
		while (iterator.hasNext()) {
			InterfacesPerfInfo interfacesPerfInfo = iterator.next();
			
			stringBuffer.append("<tr>");
			stringBuffer.append("<td>" + interfacesPerfInfo.interfaceName + "</td>");
			stringBuffer.append("<td>" + interfacesPerfInfo.nowTPS + "</td>");
			stringBuffer.append("<td>" + interfacesPerfInfo.nowTPSdelay + "</td>");
			stringBuffer.append("<td>" + interfacesPerfInfo.lastTPS + "</td>");
			stringBuffer.append("<td>" + interfacesPerfInfo.lastTPSdelay + "</td>");
			stringBuffer.append("<td>" + interfacesPerfInfo.expectTPS + "</td>");
			stringBuffer.append("</tr>");
			
		}
		
		stringBuffer.append("</table>");
		stringBuffer.append("</div>");
		
		return stringBuffer.toString();
	}
}
