package utils;

public class MyDouble implements Comparable<Double>{

	Double value = 0.0;
	
	public MyDouble(Double value) {
		
		this.value = value;
	}
	
	@Override
	public int compareTo(Double o) {
		
		if (this.value > o) {
			return 1;
		} else if (this.value == o) {
			return 0;
		}
		return -1;
	}	
}
