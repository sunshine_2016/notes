package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class OffLineMode
 */
@WebServlet("/OffLineMode")
public class OffLineMode extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OffLineMode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		response.getWriter().append("<html><head>");
		response.getWriter().append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
		response.getWriter().append("<style>"
				+ "#jz{ margin:0 auto; width:400px; height:200px; border:1px solid #4A6EAA}"
				+ "a{text-decoration: none;font-family: 宋体;font-size: 20px;color: #4A6EAA;}"
				+ "a.atag:hover {color: red;}"
				+ "</style>");
		response.getWriter().append("</head>");
		response.getWriter().append("<body>");
		
		HttpSession session = request.getSession();
		session.setAttribute("mode", "offline");
		
		// request.getRequestDispatcher("/index.jsp").forward(request, response);
		response.sendRedirect("/PerfAutoTest/index.jsp");
		
		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
